/**
 * Queries can be sent as an HTTP GET request:
 */
import { GraphQLClient, gql } from "graphql-request";
import { megamindResponse } from "../interface/megamind-response.interface";

const endpoint = `http://localhost:4000/graphql`;

const megamindSearchQuery = gql`
  {
    jira(search: "fpm") {
      issues {
        fields {
          description {
            content {
              type
              content {
                text
                type
              }
            }
          }
        }
        key
      }
    }
    sourceGraph(search: "fpm") {
      data {
        search {
          results {
            repositories {
              description
              url
            }
          }
        }
      }
    }
    confluence(search: "fpm") {
      results {
        _expandable {
          space
        }
        _links {
          webui
        }
        title
        type
      }
    }
  }
`;

const graphQLClient = new GraphQLClient(endpoint, {
  method: `GET`,
  jsonSerializer: {
    parse: JSON.parse,
    stringify: JSON.stringify,
  },
});

export const megamindSearch = async (searchString: String) => {
  const variables = {
    search: searchString,
  };
  const data: megamindResponse = await graphQLClient.request(
    megamindSearchQuery,
    variables
  );
  console.log(data);
  return data;
};
