import sourcegraph from "./source-graph.interface";
export interface megamindResponse {
  sourceGraph: sourcegraph;
}
