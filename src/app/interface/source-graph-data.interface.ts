import sourceGraphSearchInterface from "./source-graph-search.interface";

export default interface sourceGraphDataInterface {
  data: sourceGraphSearchInterface;
}
