import sourceGraphDataInterface from "./source-graph-data.interface";

export default interface sourcegraphInterface {
  data: sourceGraphDataInterface;
}
