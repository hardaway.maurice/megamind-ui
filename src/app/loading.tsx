import { Fragment } from "react";

export default function Loading() {
  return (
    <Fragment>
      <i
        style={{
          borderRadius: "50%",
          width: 50,
          height: 50,
          display: "block",
          background: `url('../assets/images/megamind-no-bg.png')`,
          backgroundPosition: "center",
          backgroundSize: "auto 300px",
        }}
      />
    </Fragment>
  );
}
