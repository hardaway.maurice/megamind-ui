import Avatar from "@mui/material/Avatar";
import { Person } from "@mui/icons-material";
import TextField from "@mui/material/TextField";
import SearchDetails from "./search-details/search-details";
import { Suspense } from "react";
import Loading from "./loading";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className="">
        <div className="">
          <Avatar>
            <Person />
          </Avatar>
        </div>
      </div>

      <div className="">
        <h1 className="text-2xl font-semibold">Megamind</h1>
        <Suspense fallback={<Loading />}>
          <SearchDetails />
        </Suspense>
        {/* <TextField sx={{ width: '90%' }} id="filled-basic" label="Search" variant="filled" /> */}
      </div>

      <div className="mb-32 grid text-center lg:mb-0 lg:w-full lg:max-w-5xl lg:grid-cols-4 lg:text-left"></div>
    </main>
  );
}
