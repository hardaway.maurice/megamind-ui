"use client";
import { Fragment, useState } from "react";
import { FormEvent } from "react";
import { megamindSearch } from "../client/gql-client";
import { TextField } from "@mui/material";

export default function SearchDetails() {
  const [resultText, setResultText] = useState("");
  const [searchText, setSearchText] = useState("");
  const [isLoading, setIsLoading] = useState("false");

  async function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();

    const formData = new FormData(event.currentTarget);
    const response = await megamindSearch(searchText);

    // Handle response if necessary
    // const jiraData = response.;
    const sourceGraphData = response.sourceGraph;
    // const confluenceData = response.confluence;

    setResultText(JSON.stringify(response));
  }

  function handleTextChange(e) {
    setSearchText(e.target.value);
  }

  return (
    <Fragment>
      <form onSubmit={onSubmit}>
        <TextField
          name="search"
          sx={{ width: "90%" }}
          onInput={handleTextChange}
          id="filled-basic"
          label="Search"
          variant="filled"
          required
        />
        <button type="submit">Submit</button>
      </form>
      <p>{resultText}</p>
    </Fragment>
  );
}
